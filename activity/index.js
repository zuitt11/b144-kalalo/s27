const http = require('http')


let directory = [
{
"firstName": "Mary Jane",
"lastName": "Dela Cruz",
"mobileNo": "09123456789",
"email": "mjdelacruz@mail.com",
"password": 123
},
{
"firstName": "John",
"lastName": "Doe",
"mobileNo": "09123456789",
"email": "jdoe@mail.com",
"password": 123
}
]

http.createServer((req, res)=>{
	if (req.url == '/profile' && req.method == 'GET') {
		res.writeHead(200, {'Content-Type': '_application/json'})
		res.write(JSON.stringify(directory) );
		res.end()
	}
	if (req.url == '/profile' && req.method == 'POST') {
	let requestBody = ''
	req.on('data', function(data){
		requestBody+=data
	})
	req.on('end', function() {
		console.log(typeof requestBody)
			requestBody = JSON.parse(requestBody)

			let newProfile = {
				"firstName": requestBody.firstName,
				"lastName": requestBody.lastName,
				"mobileNo": requestBody.mobileNo,
				"email": requestBody.email,
				"password": requestBody.password
			}
				directory.push(newProfile)
				console.log(directory)
			
				res.writeHead(200, {'Content-Type': '_application/json'});
				res.write(JSON.stringify (newProfile));
				res.end();
	})
	}
}).listen(4000)

console.log("Server runing at localhost:4000")