const http = require('http')


//mock database

let directory= [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Jobert",
		"email": "jobert@mail.com"
	},
]


http.createServer((req, res)=>{
//rooute for returning all items upon receiving a get req
	if (req.url == '/users' && req.method == 'GET') {
		res.writeHead(200, {'Content-Type': '_application/json'})
		res.write(JSON.stringify(directory) );
		res.end()
	}
	if (req.url == '/users' && req.method == 'POST') {
		//declare and initialize requestBody variable to an empty string
		let requestBody = ''

		// stream - sequence of data
			
			// dataStream - data is received from the client and is processed in the 'data' stream called 'data' the code will be triggered

			//end step - only runs after the request has completely been sent (once the data has already been processed)
		req.on('data', function(data){
			//assigns the data retrieved from the data stream to requestBody
			requestBody += data;
		})
		req.on('end', function() {
			//check if the requestBody is of data type STRING; data type JSON is needed to acces its properties
			console.log(typeof requestBody)
			requestBody = JSON.parse(requestBody)

			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			}
			//add the newUser into the database
			directory.push(newUser)
			console.log(directory)
	
			res.writeHead(200, {'Content-Type': '_application/json'});
			res.write(JSON.stringify (newUser));
			res.end();
		})
	}

}).listen(4000)

console.log("Server runing at localhost:4000")