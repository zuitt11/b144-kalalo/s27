const http = require("http")

const port = 4000

const server = http.createServer((req, res)=>{

	//http method of the incoming requests can be accessed via the 'method' property of the 'request' parameter;
		//GET Method - retrieving/reading information;default method
	if (req.url === "/items" && req.method === "GET") {
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('Data retrieved from the database')
	}
	if (req.url == "/items" && req.method == "POST"){
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('Data to be sent to the database')
	}
	if (req.url == "/items" && req.method == "PUT"){
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('Update resources')
	}
	if (req.url == "/items" && req.method == "DELETE"){
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('Delete resources')
	}
})

server.listen(port);

console.log(`server now accessible at localhost:${port}`)